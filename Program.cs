﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using Microsoft.IdentityModel.Clients.ActiveDirectory;

namespace ClientCredentials {
  class Program {
    private static void GetAccessToken() {
      AuthenticationResult result = null;
      int retryCount = 0;
      bool retry = false;

      do {
        retry = false;
        try {
          result = new AuthenticationContext(Settings.Instance.OAuth.Authority, false).AcquireTokenAsync(
            Settings.Instance.OAuth.Service.Id,
            new ClientCredential(Settings.Instance.OAuth.Client.Id, Settings.Instance.OAuth.Client.Secret)).Result;
        } catch(AdalException ex) {
          if(ex.ErrorCode == "temporarily_unavailable") {
            retry = true;
            retryCount++;
            Thread.Sleep(5000);
          }
          Console.WriteLine($"An error occurred while acquiring access token\nTime: {DateTime.Now}\nError: {ex.Message}\nRetry: {retry}\n");
        } catch(AggregateException ex) {
          foreach(Exception inner in ex.InnerExceptions) {
            Console.WriteLine(inner.Message);
          }
        } catch(Exception ex) {
          Console.WriteLine(ex.Message);
        }
      } while(retry && retryCount < 3);

      if(result == null) {
        Console.WriteLine("Canceling attempt to obtain access token.\n");
        return;
      }
      OpenBrowser($"https://jwt.io/?value={result.AccessToken}");
    }

    private static bool OpenBrowser(string url) {
      if(RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) {
        Process.Start(new ProcessStartInfo("cmd", $"/c start {url.Replace("&", "^&")}") { CreateNoWindow = true });
        return true;
      } else if(RuntimeInformation.IsOSPlatform(OSPlatform.Linux)) {
        Process.Start("xdg-open", url);
        return true;
      } else if(RuntimeInformation.IsOSPlatform(OSPlatform.OSX)) {
        Process.Start("open", url);
        return true;
      }
      return false;
    }

    static void Main(string[] args) {
      GetAccessToken();
    }
  }
}