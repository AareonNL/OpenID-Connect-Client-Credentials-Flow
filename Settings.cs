﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;
using ClientCredentials.SettingsClass;

namespace ClientCredentials {
  internal class Settings {
    private static Settings _instance;

    public static Settings Instance { get { return _instance ?? (_instance = new Settings()); } }

    private OAuth _oAuth;

    private Settings() {
      _oAuth = Configure<OAuth>("OAuth.xml");
    }

    private T Configure<T>(string xmlFilePath, bool optional = false, bool reloadOnChange = true) where T : class, new() {
      return new ConfigurationBuilder()
        .SetBasePath(System.AppContext.BaseDirectory)
        .AddXmlFile(xmlFilePath, optional, reloadOnChange)
        .Build().Get<T>();
    }

    public OAuth OAuth { get { return _oAuth; } }
  }
}
