﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientCredentials.SettingsClass {
  internal class Client {
    public string Id { get; set; }
    public string Secret { get; set; }
  }

  internal class Service {
    public string Id { get; set; }
  }

  internal class OAuth {
    public string Authority { get; set; }
    public Client Client { get; set; }
    public Service Service { get; set; }
  }
}
